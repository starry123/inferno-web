#  幽冥地府
## 登录页
![avatar](Doc/img/login.png)


## 首页
![avatar](Doc/img/index.png)


## 生死簿
![avatar](Doc/img/obituary.png)


## 勾魂管理
![avatar](Doc/img/soul.png)


## 审判记录
![avatar](Doc/img/trial.png)


## 设备管理
![avatar](Doc/img/device.png)


## 用户管理
![avatar](Doc/img/user.png)


## 地狱说明
![avatar](Doc/img/work.png)


## 六道轮回
![avatar](Doc/img/lunhui.png)


## 冥币管理
![avatar](Doc/img/GMB.png)
![avatar](Doc/img/gmb2.png)
![avatar](Doc/img/gmb3.png)
## 日志管理
![avatar](Doc/img/journal.png)


## 地狱主宰者
![avatar](Doc/img/admin.png)

## 地狱流转图
![avatar](Doc/img/流程图.png)