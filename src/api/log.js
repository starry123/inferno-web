
import { axios } from '@/utils/service'


export async function OpLoguser (parameter) {
    return axios({
        url: "/sysOpLog/userpage",
        method: "get",
        params: parameter
    })
}
