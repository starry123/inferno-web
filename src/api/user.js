import Vue from 'vue'
import { METHOD, removeAuthorization } from '@/utils/request'
import { axios, ACCESS_TOKEN } from '@/utils/service'
/**
 * 登录服务
 * @param name 账户名
 * @param password 账户密码
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function login (name, password) {
    var params = {
        Account: name,
        Password: password
    };
    return axios({
        url: "/login",
        method: "POST",
        params
    })
}

export async function getRoutesConfig () {
    return axios({
        url: "/routes",
        method: "GET"
    })
}
export async function getLoginUser () {
    return axios({
        url: "/auth/getLoginUser",
        method: "GET"
    })
}
/**
 * 退出登录
 */
export function logout () {
    localStorage.removeItem(process.env.VUE_APP_ROUTES_KEY)
    localStorage.removeItem(process.env.VUE_APP_PERMISSIONS_KEY)
    localStorage.removeItem(process.env.VUE_APP_ROLES_KEY)
    RemoveToken()
    removeAuthorization()
}



export const getList = (params) => {
    // return request("/sysuser/list", METHOD.GET, params)
    return axios({
        url: "/sysuser/list",
        method: METHOD.get,
        params
    })
}
const GetToken = () => {
    return Vue.ls.get(ACCESS_TOKEN);
}
export async function SetToken (data) {
    Vue.ls.set(ACCESS_TOKEN, data);
}
export async function RemoveToken () {
    Vue.ls.remove(ACCESS_TOKEN);
}
export default {
    login,
    logout,
    getRoutesConfig, GetToken, SetToken
}


