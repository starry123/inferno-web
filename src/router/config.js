import TabsView from '@/layouts/tabs/TabsView'
// import BlankView from '@/layouts/BlankView'
import PageView from '@/layouts/PageView'

// 路由配置
const options = {
    routes: [
        {
            path: '/login',
            name: '登录页',
            component: () => import('@/views/login/index')
        },

        {
            path: '/',
            name: '首页',
            component: TabsView,
            redirect: '/login',
            children: [
                {
                    path: 'home',

                    name: '工作台',
                    meta: {
                        icon: 'home',
                        page: {
                            closable: false
                        }
                    },
                    component: () => import('@/views/home'),
                },
                {
                    path: 'hoobituaryme',
                    name: '生死簿',
                    meta: {
                        icon: 'read',
                        page: {
                            closable: false
                        }
                    },
                    component: () => import('@/views/hoobituaryme'),
                },
                {
                    path: 'soul',
                    name: '勾魂管理',
                    meta: {
                        icon: 'schedule',
                        page: {
                            closable: false
                        }
                    },
                    component: () => import('@/views/soul'),
                },
                {
                    path: 'trial',
                    name: '审判记录',
                    meta: {
                        icon: 'dashboard',
                        page: {
                            closable: false
                        }
                    },
                    component: () => import('@/views/trial'),
                },
                {
                    path: 'trials',
                    component: PageView,
                    name: '十八层地狱',
                    meta: {
                        icon: 'bank'
                    },
                    children: [
                        {
                            path: 'device',
                            name: '设备管理',
                            meta: {
                                page: {
                                    closable: false
                                }
                            },
                            component: () => import('@/views/device')
                        },
                        {
                            path: 'user',
                            name: '用户管理',
                            meta: {
                                page: {
                                    closable: false
                                }
                            },
                            component: () => import('@/views/user')
                        },
                        {
                            path: 'work',
                            name: '地狱说明',
                            meta: {
                                page: {
                                    closable: false
                                }
                            },
                            component: () => import('@/views/work')
                        },
                    ]
                },
                {
                    path: 'lunhui',
                    name: '六道轮回',
                    meta: {
                        icon: 'interaction',
                        page: {
                            closable: false
                        }
                    },
                    component: () => import('@/views/lunhui'),
                },
                {
                    path: 'gmb',
                    name: '冥币管理',
                    meta: {
                        icon: 'transaction',
                        page: {
                            closable: false
                        }
                    },
                    component: () => import('@/views/gmb'),
                },
                {
                    path: 'oplog',
                    name: '日志管理',
                    meta: {
                        icon: 'switcher',
                        page: {
                            closable: false
                        }
                    },
                    component: () => import('@/views/oplog'),
                },

                {
                    path: 'setting',
                    name: '管理设置',
                    meta: {
                        icon: 'setting'
                    }, component: PageView,
                    children: [
                        {
                            path: 'admin',
                            name: '管理员',
                            meta: {
                                page: {
                                    closable: false
                                }
                            },
                            component: () => import('@/views/admin')
                        },
                        {
                            path: 'role',
                            name: '角色管理',
                            meta: {
                                page: {
                                    closable: false
                                }
                            },
                            component: () => import('@/views/role')
                        },

                    ]
                },
            ]
        }
    ]
}

export default options
